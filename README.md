## Login default
- vagrant

## Atividade 6 - Análise de vulnerabilidades

- Baixar e instalar o Kali Linux última versão (Máquina Virtual ou Live Pen Drive)

- Instalar a ferramenta GreenBone

- Baixar uma máquina vítima Linux ou Windows Server para ser escaneada e atacada (Vulnerability Assessment, Ataques de Denial of Service, Web Vulnerability e Password Attack)

- Mitigar as vulnerabilidades da máquina vítima, por nível de criticidade. Apresentar o roteiro de mitigação das vulnerabilidades.

Usem as ferramentas propostas pelas referências bibliográficas que julgar necessárias para as atividades.
